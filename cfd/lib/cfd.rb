require "cfd/version"

module Cfd
  PATH=File.expand_path(File.join('..', 'cfd'), __FILE__)
end

%w(
  utils
  note
  rule
  rules
  rule_set
  voice
  counterpoint
).each { |f| require File.join(Cfd::PATH, f)}
