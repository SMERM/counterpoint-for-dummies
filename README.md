# Counterpoint for Dummies

Counterpoint is generally considered an art for very intelligent people.
Only *very very* intelligent people can get good at it.

This is why the Schools of Composition around the world indulge in using books
like André Gedalge's *Traité de la Fugue*.

![](https://upload.wikimedia.org/wikipedia/commons/thumb/8/82/Andr%C3%A9_Gedalge.jpg/187px-Andr%C3%A9_Gedalge.jpg)
The *Traité de la Fugue* by André Gedalge
was intended as a multi-volume work but only the first volume, *La fugue d'école*, was produced.
It is a century-old treatise on
counterpoint and fugue which is full of rules with no explanation whatsoever.
This is because composition students are supposed to be very intelligent and
understand things without explanation.

For those who, like me, are less endowed with sparkling intelligence, this project
would like to integrate these rules with the necessary explanations.

The project may then extend to other books and treatises.

## Status

This is a work in progress. Please come back later on to check it's status.

## License

Copyright(c) 2015 Nicola Bernardini

This work is licensed under the terms of CC-BY SA 3.0
